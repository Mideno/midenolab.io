---
layout: writeup

title: Reverse200-3
difficulty: Easy 
points: 200
categories: [ reversing ]
tags: [ reversing ]

flag: poctf{uwsp_1_4m_7h3_0c34n}

---
## Hightlighted techniques

## Learning the game

We are presented with a file called `Reverse`.
I run the `file` command against it:

`Reverse200-3: ELF 64-bit LSB executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, BuildID[sha1]=932c681f4a9c2be824bce27856fe7ee8212bb7f1, for GNU/Linux 3.2.0, not stripped`

it seems to be an ELF file, so I'll be running it with my docker debian container.

```
## ./Reverse200-3
Enter the correct input: sadfsa
Incorrect input! Try again.
```

## Playing the game

To begin this challenge I used the `strings` command and found a few interesting things

```text
...
H=@@@
*REDACTEH
optc{fwuH
ps1_4__mH
_4__mh7_H
3c043}n
Correct!
Enter the correct input: 
%29s
Incorrect input! Try again.
;*3$"
...
```

So we have what looks like the flag but a bit scrambled, this is the fixed string

`optc{fwups1_4__m_4__mh7_3c043}n`

it looks like the characters are switched in pairs of two, another clue of this is what we get when trying to solve this with angr

![img_24.png](img_24.png)

looks like the `*REDACTED*` string with characters switched, so let's try that.

I got: `poctf{uwsp_1_4m_4___hm_7c340}3n`

Hmmm, so it looks like the flag starts fine, but then it kinda does not make sense.

What I realize is that the last `n` should switch places with the `}` but it is not doing so, this gives me an idea, let's replace half the characters starting from the left and the other half from the right

`poctf{uwsp_1_4m_7h3_0c34n}`

Nicee it worked, here's the final script used.

```python
scrambled_flag = "optc{fwups1_4__m_4__mh7_3c043}n"

def unscramble(flag):
    unscrambled_left = ""
    unscrambled_right = ""
    for i in range(0, len(flag) // 2, 2):
        unscrambled_left += "".join(reversed(flag[i:i + 2]))
    for i in range(len(flag)+1, (len(flag) // 2) + 6, -2):
        unscrambled_right = flag[i:i-2:-1] + unscrambled_right

    return unscrambled_left + unscrambled_right


print(unscramble(scrambled_flag))
```