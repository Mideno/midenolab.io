---
layout: writeup

title: Reverse200-2
difficulty: Easy
points: 200
categories: [ reversing ]
tags: [ reversing ]

flag: poctf{uwsp_7h3_n16h7_15_d4rk}

---

## Hightlighted techniques

- symbolic execution doesn't work
- still using angr to get data dynamically
- reversing said data

## Learning the game

We are presented with a file called `Reverse200-2`.
I run the `file` command against it:

`Reverse200-2: ELF 64-bit LSB executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, BuildID[sha1]=11b0463ea4dbbd923c4513ffb7da9e6d5bf1cfb0, for GNU/Linux 3.2.0, not stripped`

it seems to be an ELF file, so I'll be running it with my docker debian container.

```
## ./Reverse200-2
Enter the correct input: asdfasdf
Incorrect input. Try again.
```

Asks for an input and then says it's incorrect

## Playing the game

I tried going for the simple angr technique, but it is not able to solve the correct input, so I used angr but not to
directly get the correct input

Looking at the file in ghidra we find that there's a function `check_input()` which does the following

- creates a variable with a static value
- gets user input into another variable
- transforms user input with a call to `transform()`
- compares the transformed user input to the first variable

![img_22.png](img_22.png)

We quickly realize that the transformed user input has to match the static variable which we already have so the
following steps are:

- reverse the `transform()` function
- apply the inverse of `transform()` to `local_28`

The `transform()` function is really simple, for every character, it XORs it with `0x3f` and then adds `5`

![img_23.png](img_23.png)

I made Python script to reverse this

```python
def solve(encoded_flag: bytes):
    decoded_flag = ""
    for b in encoded_flag:
        decoded_b = chr((b - 5) ^ 0x3f)
        decoded_flag += decoded_b
    return decoded_flag
```

Now we need to get the static byte string from the program, for this I will show how you can do it in an interesting way
using angr.

### Get values from memory using angr

First we need to get the assembly of the program, for that we will use objdump

`objdump -d Reverse200-2 > dump.s`

In the assembly we look for the memory address where the desired string is already stored in memory.
To achieve this I looked for the call to `strcmp` so I can read the value from the argument

```objdump
...
401236:	48 8d 45 c0          	lea    -0x40(%rbp),%rax
40123a:	48 89 c7             	mov    %rax,%rdi
40123d:	e8 34 ff ff ff       	call   401176 <transform>
401242:	48 8d 55 e0          	lea    -0x20(%rbp),%rdx
401246:	48 8d 45 c0          	lea    -0x40(%rbp),%rax
40124a:	48 89 d6             	mov    %rdx,%rsi
40124d:	48 89 c7             	mov    %rax,%rdi
401250:	e8 1b fe ff ff       	call   401070 <strcmp@plt>
401255:	85 c0                	test   %eax,%eax
...
```

We see that pointers to the arguments are stored in `rsi` and `rdi`, to know which is which we can also see a bit
further up that the argument to `transform()` (which is the user input) is stored in `rbp - 0x40`, a pointer to this is
later being moved to `rax`
and finally to `rdi` before `strcmp()`, meaning that the other argument is the static string `local_28`

```python
import angr


def get_encoded_flag():
    project = angr.Project("Reverse200-2", auto_load_libs=False)

    simgr = project.factory.simgr()

    result = simgr.explore(find=0x401250)

    if result.found:
        state: angr.SimState = result.found[0]
        return state.solver.eval(state.memory.load(state.regs.get("rsi"), 29), cast_to=bytes)
```

I'll explain the important lines of this script

```python
project = angr.Project("Reverse200-2", auto_load_libs=False)
simgr = project.factory.simgr()

result = simgr.explore(find=0x401250)
```

The first two lines simply get the simulation manager as usual.

The next line looks for a state located in the call to `strcmp()`

```python
if result.found:
    state: angr.SimState = result.found[0]
    return state.solver.eval(state.memory.load(state.regs.get("rsi"), 29), cast_to=bytes)
```

if a state is found, we get the value from memory, lets break up that final line a little bit

```python
rsi_value = state.regs.get("rsi")
unsolved_value = state.memory.load(rsi_value, 29)
return state.solver.eval(unsolved_value, cast_to=bytes)
```

- the `rsi` register holds a pointer to the string we want, so by doing `state.regs.get("rsi")` we get that pointer as a
  memory address
- we load the value at that address into a variable with `state.memory.load` which takes two arguments
    - a memory address (we use the pointer from `rsi` here)
    - an integer representing how many bytes we want to read
- `state.solver.eval` is used to solve the value, since `state.memory.load` returns a `BV` object (there doesn't seem to
  be an easier way to read the value as a string even if it is solved)

Here's the whole code

```python
import angr

def get_encoded_flag():
    project = angr.Project("Reverse200-2", auto_load_libs=False)


    simgr = project.factory.simgr()
    simgr.use_technique(angr.exploration_techniques.DFS())

    result = simgr.explore(find=0x401250)

    if result.found:
        state: angr.SimState = result.found[0]
        return state.solver.eval(state.memory.load(state.regs.get("rsi"), 29), cast_to=bytes)


def solve(encoded_flag: bytes):
    decoded_flag = ""
    for b in encoded_flag:
        decoded_b = chr((b - 5) ^ 0x3f)
        decoded_flag += decoded_b
    return decoded_flag


flag = solve(get_encoded_flag())

print(flag)

```