---
layout: writeup

title: Reverse300-1
difficulty: Easy
points: 300
categories: [ reversing ]
tags: [ reversing ]

flag: poctf{uwsp_7h3_w0rld_15_4_57463}
---

## Hightlighted techniques

- symbolic execution with angr

## Learning the game

We are presented with a file called `Reverse300-1`.
I run the `file` command against it:

`Reverse300-1: ELF 64-bit LSB executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, BuildID[sha1]=cd45573f4bd7b1d2d713912994eec4d881dfb71f, for GNU/Linux 3.2.0, not stripped`

it seems to be an ELF file, so I'll be running it with my docker debian container.

```
## ./Reverse300-1
Enter the key to decrypt the flag: sadfasdf
Incorrect key length. Key must be 22 characters long.
```

## Playing the game

Let's check the functions in an objdump disassembly.

```text
...
00000000004012e0 <decrypt_flag>:
  4012e0:	55                   	push   %rbp
  4012e1:	48 89 e5             	mov    %rsp,%rbp
  4012e4:	48 83 ec 40          	sub    $0x40,%rsp
  4012e8:	48 89 7d c8          	mov    %rdi,-0x38(%rbp)
  4012ec:	48 b8 14 0c 0b 12 11 	movabs $0x16302911120b0c14,%rax
  4012f3:	29 30 16 
  4012f6:	48 ba 14 05 0f 7d 50 	movabs $0x212f12507d0f0514,%rdx
...
```

We see that there's a `decrypt_flag()` function which is probably called when the password is correct, so let's use angr
to look for a state at the start of this function and then get `stdin` as a string

```python
import angr

def get_key():
    project = angr.Project("Reverse300-1", auto_load_libs=False)


    simgr = project.factory.simgr()

    result = simgr.explore(find=0x4012e0)

    if result.found:
        state: angr.SimState = result.found[0]
        return state.posix.dumps(0)


print(get_key())
```

This outputs: `b'dchfwREaguPJ8!pV*^U&Ms'`

okay then, let's use that as our password

```text
# ./Reverse300-1 
Enter the key to decrypt the flag: dchfwREaguPJ8!pV*^U&Ms
The flag is: poctf{uwsp_7h3_w0rld_15_4_57463}
```
