---
layout: writeup

title: Reverse100-2
difficulty: easy
points: 100
categories: [ reversing ]
tags: [ reversing ]

flag: poctf{uwsp_d0_0r_d0_n07}

---
## Hightlighted techniques

- symbolic execution with angr

## Learning the game

We are presented with a file called `Reverse100-2`.
I run the `file` command against it:

`Reverse100-2: ELF 64-bit LSB executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, BuildID[sha1]=48d49fdc0741aa19b895d5fec898bd484d2eb49e, for GNU/Linux 3.2.0, not stripped`

it seems to be an ELF file, so I'll be running it with my docker debian container.

```
## ./Reverse100-2
Enter the password: asdasd
Access denied!
```

The program asks for a password and later displays the `Access denied!` text.

## Playing the game

Okay so this structure of challenge tempts me to go for an easy angr solve, but then when I looked at the output of
the `strings` command looking for the target text to use for stdout I see this:

```text
...
poctf{uwH
sp_d0_0rH
p_d0_0r_H
d0_n07}
Enter the password: 
%99s
Access granted!
Access denied!
...
```

sooo, the flag seems to already be there, this is that string fixed in a single line:

`poctf{uwsp_d0_0rp_d0_0r_d0_n07}`

Hm, looks kinda weird, we can probably guess the flag from here but let's just do a simple angr script to make sure.

```python
import angr

project = angr.Project("Reverse100-2", auto_load_libs=False)

simgr = project.factory.simgr()

print(simgr.explore(find=lambda state: b"granted" in state.posix.dumps(1)).found[0].posix.dumps(0))
```

```text
b'poctf{uwsp_d0_0r_d0_n07}\x00\x00\x00\x00...\x00'
```

It works!! (with some extra trailing `\x00`)

