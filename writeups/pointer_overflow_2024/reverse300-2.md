---
layout: writeup

title: Reverse300-2
difficulty: Medium
points: 300
categories: [ reversing ]
tags: [ reversing ]

flag: poctf{uwsp_4b4nd0n_4ll_h0p3}

---

## Hightlighted techniques

- procmon
- using burpsuite as proxy

## Learning the game

We are presented with a file called `Reverse`.
I run the `file` command against it:

`Reverse300-2.exe: PE32+ executable (console) x86-64, for MS Windows, 10 sections`

```
## ./Reverse300-2.exe
Fetching the flag from a secure source...
Success!
```

## Playing the game

Okay so I got kinda stuck in the beginning for this one I'll admit, I tried looking at the program in ghidra and looking
for where the `Success!` string was being printed, but I couldn't find it.

Finally, I decided that the *"secure source"* from where the flag was being gotten had to be one of two:

- a hidden file created at runtime
- some remote server

So I looked at what the program does with procmon.

![img_26.png](img_26.png)

After applying a few filters to make it easier we can see that the program is clearly performing some http request, so I
will use burpsuite to intercept the data for this request

![img_25.png](img_25.png)

