---
layout: writeup

title: Reverse100-3
difficulty: easy
points: 100
categories: [ reversing ]
tags: [ reversing ]

flag: poctf{uwsp_br3v17y_15_7h3_50u1}

---
## Learning the game

We are presented with a file called `Reverse100-3`.
I run the `file` command against it:

`Reverse100-3: ELF 64-bit LSB executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, BuildID[sha1]=a372e4d793f897e5a6c3382036f11a4a12f029a2, for GNU/Linux 3.2.0, not stripped`

it seems to be an ELF file, so I'll be running it with my docker debian container.

```
## ./Reverse100-3
Encoded flag: Flag after reverse step 0: 8e79a99cacd5c5c7917aa58ab88dc6815583a5597bb987b851697b58bb8bcd
Decode function not added yet!Decoded flag (plaintext in hex): Flag after reverse step 0: 8e79a99cacd5c5c7917aa58ab88dc6815583a5597bb987b851697b58bb8bcd
```

We are also provided with the following source code

```c
#include <stdio.h>
#include <string.h>

// Convert each byte of the flag to hex and print it
void print_flag_hex(unsigned char *flag, int length, int step) {
    printf("Flag after reverse step %d: ", step);
    for (int i = 0; i < length; i++) {
        printf("%02x", flag[i]);  // Print each byte in hexadecimal
    }
    printf("\n");
}

// Reverse the modification of the flag bytes based on the seed
void reverse_modify_flag(unsigned char *flag, unsigned int seed) {
    int length = strlen((char *)flag);

    for (int i = 0; i < length; i++) {
        flag[i] = (flag[i] - (seed % 10)) % 256;  // Reverse each byte modification
        seed = seed / 10;
        if (seed == 0) {
            seed = 88974713;  // Reset seed if it runs out
        }
    }
}

int main() {
    unsigned char encoded_flag[] = { 0x8e, 0x79, 0xa9, 0x9c, 0xac, 0xd5, 0xc5, 0xc7, 0x91, 0x7a, 0xa5, 0x8a, 0xb8, 0x8d, 0xc6, 0x81, 0x55, 0x83, 0xa5, 0x59, 0x7b, 0xb9, 0x87, 0xb8, 0x51, 0x69, 0x7b, 0x58, 0xbb, 0x8b, 0xcd};

    unsigned int seed = 88974713;
    int length = sizeof(encoded_flag);

    printf("Encoded flag: ");
    print_flag_hex(encoded_flag, length, 0);

    // Reverse the modifications 10 times (finish this!)
    printf("Decode function not added yet!");

    printf("Decoded flag (plaintext in hex): ");
    print_flag_hex(encoded_flag, length, 0);  // Print final decoded flag in hex

    return 0;
}
```

## Playing the game

Seems like we just need to modify the source code to call the reversing function in a loop

```c
...
int main() {
    unsigned char encoded_flag[] = { 0x8e, 0x79, 0xa9, 0x9c, 0xac, 0xd5, 0xc5, 0xc7, 0x91, 0x7a, 0xa5, 0x8a, 0xb8, 0x8d, 0xc6, 0x81, 0x55, 0x83, 0xa5, 0x59, 0x7b, 0xb9, 0x87, 0xb8, 0x51, 0x69, 0x7b, 0x58, 0xbb, 0x8b, 0xcd};

    unsigned int seed = 88974713;
    int length = sizeof(encoded_flag);

    printf("Encoded flag: ");
    print_flag_hex(encoded_flag, length, 0);

    for(int i = 0; i < 10; i++){
        reverse_modify_flag(encoded_flag, seed);
            printf("Decoded flag (plaintext in hex): ");
        print_flag_hex(encoded_flag, length, i);  // Print final decoded flag in hex
    }

    return 0;
}
```

We compile and run this...

```text
##.\a.exe
Encoded flag: Flag after reverse step 0: 8e79a99cacd5c5c7917aa58ab88dc6815583a5597bb987b851697b58bb8bcd
Decoded flag (plaintext in hex): Flag after reverse step 10: 706f6374667b757773705f627233763137795f31355f3768335f353075317dcf00000079a54d050a0000008000400010ff610054ff610088124000010000008815c1008822c100fdffffff020000000000000054ff6100cd88c5750070360084ff6100f512400001000000000000000000000000000000000000000000000000000000a97bd97500703600907bd975dcff6100cbc0cd770070360081430b0d000000000000000000703600000000000000000000000000000000000000000000000000000000000000000000000000
```

Then copy whatever this is into cyberchef to get a string

![img_15.png](img_15.png)
