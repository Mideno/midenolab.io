---
layout: writeup

title: Flag Keygen
difficulty: moderate
points: 190
categories: [ reversing ]
tags: [ reversing ]

flag:
  user: 247CTF{fb88b9fe80e969e73a27541f62d6f89c}

---

## Challenge

### Description

We created a service which can read and print the flag for you. To use the application, you first need to enter a valid
product key. Can you reverse the algorithm and generate a valid key?

## Solution

Seems like the program contains some kind of algorithm to check a key that's provided by the user, we will reverse the
program to find a valid key

### Initial static analysis

First of all we can see that the program asks for a product key and only runs `FUN_00101195` if it is valid

![img.png](img.png)

The function `check_product_key` takes a pointer to a string (the product key) and it performs a series of checks to it

![img_1.png](img_1.png)

### Simple constraints

At first sight we can find two constraints:

- The key has to be of 32 characters long
- The only allowed characters are uppercase letters

The next part is more complicated

### Weird ASCII math

![img_2.png](img_2.png)

![img_3.png](img_3.png)

The function I so wisely renamed `n_181_else_177` adds 181 to a character if it is less than `N` or 177 otherwise, then
it returns the result

We can see this function being used in two places.

First:

1. we are iterating the product key starting from the second value
2. applying the aforementioned function to the value on the current index
3. substracting the index from the result
4. adding 247 to it
5. adding it to an accumulator (`local_20`) with a starting value of 247

Second:

1. We apply `n_181_else_177` to the first value of the product key

After this two transformations we use them in an if statement

![img_4.png](img_4.png)

### First character value

This if statement is telling us that `B_plus181` has to be equal to 247, we can check the correct value for this by
using an ASCII table.

possible values:

- 247 - 181 = 66
- 247 - 177 = 70

in order for 70 to be a valid value it has to be greater than `N` in the ASCII table

![img_5.png](img_5.png)

as we can see, 70 is not greater than `N` in the ASCII table so the correct value for the first character of the product
key is 66, which is `B`

### Complicated modulus stuff (but we cheat)

So to set `local_20` % 248 = 247 we'd have to solve the math operation and do some weird calculations (or use symbolic
execution), luckily as
programmers we can just skip all that and go for an easier approach.

I copied the `check_product_key` function and added a `printf()` to see the result of the modulus after all the
calculations

![img_6.png](img_6.png)

As we can see, going up one character goes up one value in the modulus result (this should not be like this in every
case, but it's good enough) we can use this to get the correct value by simply using the necessary characters at the end
of the key

![img_7.png](img_7.png)

In my case I got a valid product key by setting all `A`s, a `J`, and 4 `Z`s

### Send the key

To complete the challenge I got into a bit of trouble with telnet, so I simply used a python script to connect to the
server aaaaaand...

![img_8.png](img_8.png)

Okay I know, you can't see what's going on, but the python script sends the key I just showed and prints the received data.

Hope you liked this, hit me up if you want to chat.
