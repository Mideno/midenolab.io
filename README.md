# Mideno website (based on galaxians website)

## Writeups folder

The folder structure
```
writeups
├── EventTitle
│   ├── writeupfiles
│   │   ├── file1.py
│   │   ├── file2.txt
│   │   ├── file3.png
│   ├── index.md
│   ├── challenge1-title-of-chall.md
│   ├── challenge2-title-of-chall.md
│   ├── challenge3-title.md
├── AnotherEventTitle
│   ├── index.md
... ...
```

## Adding writeup
Writeups in the `writeups` folder, in a subfolder per event

Writeups will appear in alphabetical order of filename, so use a namingscheme like `challenge1-title-of-challege.md`, orso

## Adding event
To add a new event, create a folder in `writeups` with the event name.

Add an `index.md` file:

```markdown

---
layout: ctf-event
title: Title of Event
date: 2042-04-02

difficulty: false  # optional, if an event doesnt have difficulty, hide it from table
points: false      # optional, if an event doesnt have difficulty, hide it from table

cover: hacky-easter.png # image in assets/img/, defaults to galaxians logo
---

Some description of the event

```


