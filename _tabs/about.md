---
# the default layout is 'page'
icon: fas fa-info-circle
order: 6
---

I'm Mideno, my actual name is Ezequiel Fuentes Martin, since I was born in 2003 my objective in life has always been to learn, learn it all and solve every puzzle there is, that's why you'll see that my portfolio is full of projects exploring every corner of IT, also my devotion to puzzles carried on to my carrer in the form of CTFs, please come visit my writeups.

In the professional aspect I'm a focused Cybersecurity and networking professional.

| ![](https://github.com/Miden0.png?size=150)|
|**Mideno** (Ezequiel Fuentes Martin)|



