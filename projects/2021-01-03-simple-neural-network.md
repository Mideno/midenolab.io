---
title: Simple-Neural-Network
author: mideno
date: 2023-08-15 01:52:00 -0300
layout: project

cover: neuralnetwork.png
repo: https://gitlab.com/ia3802014/Simple-Neural-Network
reponame: Simple-Neural-Network
---

This project is a **simple neural network** (1 neuron) made entirely **with python and math libraries**, no neural network or AI libraries.

This is a way of **showing my knowledge** in math and neural network internals, and to show **I know how neural networks work**.